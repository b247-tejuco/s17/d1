console.log('heluu hihihi wkwkwkw')

/*[SECTION] Functions
	Functions in Javascript are lines/block of codes that tell 
	our device/application to perform a certain task when called/invoked. */

/*[SECTION] Function Declaration
			-This is the part where we defined a function with the 
			specified parameters. */

			/*
				Syntax:
					function functionName(){
						code block (statement)
					}
			*/

			// example1 1 declaration 4 invocation
			function printName(){
			console.log('My name is Voren')
			}
  
  /*[SECTION] Function Invocation
  			-This is the part where we invoke/call
  			The code block and statement inside a function is not immediately 
  			executed when the function is defined*/


			  printName();
			  printName();
			  printName();
			  printName();

			  // example2 4 declaration 1 invoke

			  function printName2(){
			  	console.log('Heloo')
			  	console.log('Heloo')
			  	console.log('Heloo')
			  	console.log('Heloo')
			  };


			 printName2();

			 	//variable Function(); //will render an error since this is a function expressions

			 //[SECTION] Function Declaration vs Expressions
			 	//[SUB-SECTION] Function Declarations

			 		//Hoisting is Javascript's behavior for certain variables\
			 		//and functions to rune or use them before their declaration.


			 	//[SUB-SECTION] Function Expressions
			 		// a function can also be stored in a variable.
			 		//this is called a function expression.
			 		
			 		let variableFunction= 'red';		
			 		console.log(variableFunction);

			 		variableFunction= function(){	//reassigning, dont use let
			 		console.log('Hello again! This is a function expression')
			 	};

			 	variableFunction();

			 //[SECTION] Function Scoping
			 	//Scope is the accessiblility (visibility) of variables
			 	//within our program.

			 		//3 types of scope
			 			// 1.local/block scope
			 			// 2. global scope
			 			// 3. function scope

			 	{
			 		let localVar= 'Wakanda';
			 		console.log(localVar)
			 	}

			 	let globalVar ='Mr. Worldwide';
			 	console.log(globalVar);

			 	let localVar= 'Not Wakanda';
			 	console.log(localVar); //localvar, being a block, 
			 	//cannot be accessed outside  of its code block.

			 	// Function Scope
			 		// Javascript has a function scope
			 		//each function creates a new scope.


			 	function showNames() {
			 		var functionVar= 'Joe';
			 		let functionLet= 'Jane';
			 		const functionConst='John';

			 		console.log(functionVar);
			 		console.log(functionLet);
			 		console.log(functionConst);
			 	};

			 	showNames();


			 	//[SECTION] Nested Functions

			 	//you can create another function inside a function.

			 	function myNewFunction(){
			 		let name= 'Mike';
			 		function nestedFunction(){
			 			let nestedName= 'Scottie'
			 			console.log(nestedName);
			 		};
			 		nestedFunction();
			 		console.log(name);
			 	};
			 	myNewFunction();
			 	//nestedFunction(); // will result to an error

			 // [SECTION] Function and Global Scoped Variables

			 	//Global Scoped Variable
			 	let globalName ='Alexandro';

			 	function MyNewFunction2(){
			 		let nameInside= 'Renz'
			 		console.log(globalName);
			 		console.log(nameInside);
			 	};

			 	MyNewFunction2();

			 // [SECTION] Using alert()
			 	//alert alows us to show a small window at top of our browser
			 	//to show information to our users.
			 	/*
			 		alert() syntax:
			 			alert('messageInString')

			 			*/

			 	alert('Mabuhay!');

			 	console.log('I will only log in the console when the alert is dismissed.');

			 	/*
			 	Notes on the use of alert():
			 	 -Show only alert() for short dialogs/messages
			 	 to the user. 
			 	 -Do not overuse alert because the program/js has to wait for it
			 	 to be dismissed before continuing.
			 	 */

			 	 //[SECTION] Using prompt()

			 	 let samplePrompt= prompt('Enter your name.');
			 	 console.log(typeof samplePrompt);
			 	 console.log(samplePrompt);
			 	 console.log('Hello, '+ samplePrompt+ "!");
			 	 	alert('Hello again, '+ samplePrompt+ "!");

			 	 	/*
						prompt() Syntax:
						prompt('dialogInString');

			 	 	*/

			 	 	// prompt() returns an empty string when there is no input. And 'null' if
			 	 	// the user cancels the prompt.

			 	 function printWelcomeMessage(){
			 	 	let firstName= prompt('Kindly enter your First Name: ')
			 	 	let lastName= prompt('Kindly enter your Last Name: ')
			 	 	console.log('Hellow, '+firstName+ " "+ lastName+"!");
			 	 	alert('Welcome to my page!');
			 	 };

			 	 printWelcomeMessage();

		//[SECTION] Function Naming Conventions

			//Function names should be definitive of the task it will perform.
			//Make it relevant. Safe for work
			// It usually contains a verb.

			function getCourses(){
				let courses=['Science 101', 'Math 101', 'English 101']
				console.log(courses);
			};
			getCourses();











